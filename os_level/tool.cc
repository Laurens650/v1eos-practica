#include <iostream>
#include <string>
using namespace std;

string translate(string line, string argument)
int arg = stoi(argument);
char letters;
for (int i = 0; line[i] != '\0'; ++i) {
	letters = line[i];
	//normale letters
	if (letters >= 'a' && letters <= 'z') {
		letters = letters + arg;
		if (letters > 'z') {
			letters = letters - 'z' + 'a' - 1;
		}
		line[i] = letters;
	}
	//hoofd letters
	else if (letters >= 'A' && letters <= 'Z') {
		letters = letters + arg;
		if (letters > 'Z') {
			letters = letters - 'Z' + 'A' - 1;
		}
		line[i] = letters;
	}
}
{ string result = line;
  return result; }

int main(int argc, char *argv[])
{ string line;

  if(argc != 2)
  { cerr << "Deze functie heeft exact 1 argument nodig" << endl;
    return -1; }

  while(std::getline(std::cin, line))
  { cout << translate(line, argv[1]) << endl; } 

  return 0; }
