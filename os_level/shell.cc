#include "shell.hh"
using namespace std;

int main()
{ string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
string prompt = "Kies een prompt die je wil gebruiken";

  while(true)
  { cout << prompt;                   // Print het prompt
    getline(cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (cin.eof()) return 0; } }      // EOF is een exit

void new_file(){ 
	string bestandsNaam = "";
	const char* naam[] = { NULL };
	const char* invoer[] = { NULL };
	string tekst = "";
	string totaal = "";

	cout << "Geef de naam van het bestand dat je wil maken" << endl;
	getline(cin, bestandsNaam);
	naam[0] = bestandsNaam.c_str();
	int fd = syscall(SYS_creat, naam[0], 0644);

	cout << "Schrijf de tekst die je in het bestand wil hebben, rond af met <EOF>" << endl;
	while (getline(cin, tekst)) {
		if (tekst == "<EOF>") {
			break;
		}
		totaal += tekst + "\n";
	}
	invoer[0] = totaal.c_str();
	syscall(SYS_write, fd, invoer[0], totaal.size());
}

void list(){ 
	int checkKind = syscall(SYS_fork);

	if (checkKind == 0) {
		const char* args[] = { "/bin/ls", "-l", "-a", NULL };
		syscall(SYS_execve, args[0], args, NULL);
	}
	else {
		syscall(SYS_wait4, checkKind, NULL, NULL, NULL);
	}
}

void find(){ 
	string zoek = "";

	cout << "Geef de naam van de bestanden die je wil zoeken" << endl;
	getline(cin, zoek);
	const char* argsFind[] = { "/usr/bin/find", ".", NULL };
	const char* argsGrep[] = { "/bin/grep", zoek.c_str(), NULL };

	int fd[2];
	syscall(SYS_pipe, &fd);
	int checkKind = syscall(SYS_fork);
	if (checkKind == 0) {
		syscall(SYS_close, fd[0]);
		syscall(SYS_dup2, fd[1], 1);
		syscall(SYS_execve, argsFind[0], argsFind, NULL);
	}
	else {
		int checkKind2 = syscall(SYS_fork);
		if (checkKind2 == 0) {
			syscall(SYS_close, fd[1]);
			syscall(SYS_dup2, fd[0], 0);
			syscall(SYS_execve, argsGrep[0], argsGrep, NULL);
		}
		else {
			syscall(SYS_close, fd[1]);
			syscall(SYS_close, fd[0]);
			syscall(SYS_wait4, checkKind, NULL, NULL, NULL);
			syscall(SYS_wait4, checkKind2, NULL, NULL, NULL);
		}
	}
}

void seek(){ 
	const char* bestanden[] = { "seek", "loop" };

	int fdSeek = syscall(SYS_creat, bestanden[0], 0644);
	syscall(SYS_write, fdSeek, "x", 1);
	syscall(SYS_lseek, fdSeek, 5000000, 1);
	syscall(SYS_write, fdSeek, "x", 1);

	int fdLoop = syscall(SYS_creat, bestanden[1], 0644);
	syscall(SYS_write, fdLoop, "x", 1);
	for (int i = 0; i < 5000000; i++) {
		syscall(SYS_write, fdLoop, "\0", 1);
	}
	syscall(SYS_write, fdLoop, "x", 1);
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
